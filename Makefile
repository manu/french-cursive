# main Makefile for French Cursive
# (c) 2002--2011 Emmanuel Beffara, LPPL
#
#----------------------------------------------------------
#
# Modify the following variables to install the files in the appropriate
# directories for your system.
#
# The base of the TeX hierarchy, for installation:

TEXMF = /usr/share/texmf

# Shall we include the Adobe Type1 version in the process?
# Non-empty means true.

WITH_TYPE1 = true

# Here ends the customization part.
#
#----------------------------------------------------------

GENERATED_FONTS = $(shell cut -d' ' -f1 generated.lst)
FONTS = $(GENERATED_FONTS) frca10 frcw10
SOURCES = frcursive.mf

GENERATED_MF = $(addsuffix .mf,$(GENERATED_FONTS))
FILES_MF = $(SOURCES) $(GENERATED_MF)
FILES_TFM = $(addsuffix .tfm,$(FONTS))
FILES_TYPE1 = $(addsuffix .pfb,$(FONTS))
FILES_LATEX = frcursive.sty ot1frc.fd t1frc.fd
FILES_DOC = README COPYING frcursive.pdf

MF = mf
MFENV = env MFINPUTS=.:$$MFINPUTS

.SUFFIXES:
.PHONY: all dist dist-tree ctan source tfm type1 latex doc clean

#--  Main targets

all: source $(if $(WITH_TYPE1),tfm type1) latex doc
dist: frcursive.tar.gz
ctan: frcursive.zip

source: $(FILES_MF)
tfm: $(FILES_TFM)
type1: $(FILES_TYPE1) frcursive.map
latex: $(FILES_LATEX)
doc: $(FILES_DOC)

clean:
	rm -f *.log *.aux *.toc *.dvi *.ps *.pdf *.*gf *.tfm *.pfb
	rm -f $(GENERATED_MF) frcursive.sty frcursive.map frcursive.tex pdftex.map

#--  Individual files

$(GENERATED_MF): %.mf: mkdrv.sh generated.lst
	sh mkdrv.sh $(shell grep ^$(subst .mf,,$@) generated.lst | cut -d' ' -f2-) > $@

%.tfm: %.mf $(SOURCES)
	$(MFENV) $(MF) '\mode:=ljfour;' input $*

%.pfb: %.mf %.tfm $(SOURCES)
	$(MFENV) mftrace -f PFB --simplify $*
frcursive.map: generated.lst
	for FONT in $(FONTS); do echo "$$FONT $$FONT <$$FONT.pfb"; done > $@

frcursive.sty: frcursive.ins frcursive.dtx
	latex --interaction=batchmode $<

pdftex.map: frcursive.map
	cat frcursive.map $(shell kpsewhich pdftex.map) > pdftex.map
frcursive.pdf: frcursive.dtx $(FILES_LATEX) $(if $(WITH_TYPE1),$(FILES_TFM) $(FILES_TYPE1) pdftex.map)
	pdflatex $<
frcursive.tex: frcursive.mf
	mft $<
fcsource.pdf: fcsource.tex frcursive.tex $(if $(WITH_TYPE1),frcr10.tfm frcr10.pfb pdftex.map)
	pdftex $<

#--  Installation

install: all
	mkdir -p $(TEXMF)/fonts/source/public/frcursive
	cp $(FILES_MF) $(TEXMF)/fonts/source/public/frcursive
	mkdir -p $(TEXMF)/tex/latex/frcursive
	cp $(FILES_LATEX) $(TEXMF)/tex/latex/frcursive
	mkdir -p $(TEXMF)/doc/fonts/frcursive
	cp $(FILES_DOC) $(TEXMF)/doc/fonts/frcursive
ifdef WITH_TYPE1
	mkdir -p $(TEXMF)/fonts/tfm/public/frcursive
	cp $(FILES_TFM) $(TEXMF)/fonts/tfm/public/frcursive
	mkdir -p $(TEXMF)/fonts/type1/public/frcursive
	cp $(FILES_TYPE1) $(TEXMF)/fonts/type1/public/frcursive
	mkdir -p $(TEXMF)/fonts/map/dvips/frcursive
	cp frcursive.map $(TEXMF)/fonts/map/dvips/frcursive
endif

#--  Distribution

dist-tree: all
	rm -rf frcursive
	mkdir frcursive
	cp $(FILES_DOC) frcursive.dtx frcursive
	mkdir frcursive/mf
	cp $(FILES_MF) frcursive/mf
	mkdir frcursive/latex
	cp $(FILES_LATEX) frcursive/latex
ifdef WITH_TYPE1
	cp frcursive.map frcursive
	mkdir frcursive/tfm
	cp $(FILES_TFM) frcursive/tfm
	mkdir frcursive/type1
	cp $(FILES_TYPE1) frcursive/type1
endif

frcursive.tar.gz: dist-tree
	tar zcf $@ frcursive

frcursive.tds.zip: all
	rm -rf tds
	$(MAKE) install TEXMF=$(shell pwd)/tds
	cd tds ; zip -qr ../$@ *
	rm -rf tds

frcursive.zip: dist-tree frcursive.tds.zip
	zip -qr $@ frcursive frcursive.tds.zip
