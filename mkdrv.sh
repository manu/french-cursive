#!/bin/sh
#
# Automatic generation of driver files for French Cursive.
#
# � Emmanuel Beffara, 2001--2011. This script is covered by the LPPL licence,
# and is part of the French Cursive font sources.

calc()
{
	( echo "scale=3"; echo $* ) | bc -l
}

DESCR=""    # textual description
IDENT=""    # font identifier
SIZE=$1     # specific size in points

EX=0        # value of ex# in sharp points
THIN=0.2    # thickness of thin drawings
MED=0.4     # thickness of medium drawings
THICK=0.6   # thickness of thick drawings
DOT=0.8     # diameter of dots

WIDTH=0.8   # base width
SLANT=0     # slant factor

ACCW=0.7    # relative width of accents

FIXED=false # fixed-thickness version ?

shift
while [ "$#" != 0 ]; do
	case $1 in
	bold)	IDENT="${IDENT}B"; MED=0.55; THICK=0.8; DOT=1.25;
		DESCR="${DESCR}bold ";;
	callig)	IDENT="${IDENT}C"; MED=0.3; THICK=0.8;
		DESCR="${DESCR}calligraphic ";;
	ext)	IDENT="${IDENT}X"; WIDTH=0.9;
		DESCR="${DESCR}extended ";;
	slant)	IDENT="${IDENT}SL"; SLANT=0.3;
		DESCR="${DESCR}slanted ";;
	fixed)	IDENT="${IDENT}F"; FIXED=true;
		THIN=$MED; THICK=$MED; DOT=`calc "2*$MED"`;
		DESCR="${DESCR}fixed-thickness ";;
	esac
	shift
done

# The value of ex# is computed the same way as for Computer Modern:
EX=`calc "$SIZE*15.5"`/36

# The base width is multiplied by a scale factor related to height
WIDTH=`calc "$WIDTH/e(l($SIZE/10)/6)"`

# Correct the thickness values according to the size:
THIN=`calc "$THIN*$SIZE/10"`
MED=`calc "$MED*$SIZE/10"`
THICK=`calc "$THICK*$SIZE/10"`
DOT=`calc "$DOT*$SIZE/10"`

# The identifier is "FRC" followed by the letters above (or "R")
IDENT="FRC${IDENT:-R}"

# And here we go...

cat <<EOF
%%% This file is part of the French Cursive font definition.
%%% This is a parameter file for the ${DESCR:-regular }variant

font_identifier := "$IDENT";
font_size $SIZE pt#;

mode_setup;

ex# := $EX pt#;
med# := $MED pt#;
thin# := $THIN pt#;
thick# := $THICK pt#;
dot_size# := $DOT pt#;

base_width := $WIDTH;
slant := $SLANT;
accent_wd := $ACCW;
link_width := 0.13;
straight_ascend := 2;
loop_ascend := 2.5;
uc_ascend := 2.5;
straight_descend := 1.2;
loop_descend := 1.5;
dot_height := 1.4;
accent_bot := 1.2;
accent_top := 1.6;
cedilla_dp = 0.2;

EOF

# When using a fixed-thickness version, we have to add special code to use
# the "draw" macro instead of "penstroke":

$FIXED && cat <<EOF
boolean fixed; fixed := true;
def penstroke text t =
  begingroup
    save e; def e = enddef;
    draw t withpen pencircle scaled med;
  endgroup
enddef;

EOF

# And the final line:
echo "input frcursive"
