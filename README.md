The French Cursive font family
==============================

This is a cursive hand-writing font family in the style of the French academic
running-hand, written with Metafont for use with the TeX typesetting system.


Installation
------------

French Cursive is available on CTAN and packaged in common distributions of TeX.

The `Makefile` in the source repository contains a target `install` that
installs the files at the right places in a standard TeX directory hierarchy.


Usage
-----

The LaTeX package `frcursive` provides en environment `cursive` and a macro
`\textcursive` that enable the French Cursive family. Several other commands
are provided to use specific variants of the font. The file `frcursive.pdf`
contains the documentation of the LaTeX package.


Feedback
--------

Any comments and suggestions are welcome, as I consider this work to be in
continuous (but very slow) developement. The sources are hosted on Framagit at
<https://framagit.org/manu/french-cursive>.

� Emmanuel Beffara <manu@beffara.org>, 2001--2011

This work is covered by the LPPL license, version 1.2.


ChangeLog
---------

2011/11/09:
 - various packaging updates
 - Type 1 version included in the distribution

2005/04/16:
 - changed the codes of link glyphs to the range 0--32
 - added all accented letters of the T1 encoding
 - less design sizes

2004/02/02:
 - added the square brackets and the slash
 - added some hinting information
 - fixed the NFSS description files to make them agree with standard drivers
 - updated the LaTeX package to give access to all variants

2003/06/19:
 - added the Spanish opening exclamation and interrogation marks
 - changed the driver names from "fc*" to "frc*" to avoid clashes with Jorg
   Knappen's FC series

2003/04/29:
 - first release on CTAN
